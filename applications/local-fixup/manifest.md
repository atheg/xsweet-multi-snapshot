

#### hyperlink-inferencer.xsl

XSLT stylesheet version 3.0 (2 templates)

XSweet: picks up URI substrings and renders them as (HTML) anchors with (purported or matched) links

Input: HTML Typescript or relatively clean HTML or XML.

Output: A copy, except that URIs now appear as live links (`a` elements).

#### ucp-fixup.xsl

XSLT stylesheet version 2.0 (2 templates)

XSweet: A demo of an "ad hoc" filter for a particular workflow or process.

Input: HTML Typescript

Output: A copy, with modifications.

#### zorba-map.xsl

XSLT stylesheet version 2.0 (3 templates)

XSweet: A demo of an "ad hoc" filter for a particular workflow or process - this time, tuned particularly for a particular usage profile.

Input: HTML Typescript

Output: A copy, with modifications.