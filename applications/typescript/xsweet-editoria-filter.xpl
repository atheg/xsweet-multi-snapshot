<?xml version="1.0" encoding="UTF-8"?>
<p:declare-step  version="1.0"
  xmlns:p="http://www.w3.org/ns/xproc"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:c="http://www.w3.org/ns/xproc-step"
  xmlns:xsw="http://coko.foundation/xsweet"
  type="xsw:xsweet-editoria-filter" name="xsweet-editoria-filter">
  
  <!-- Implementing a mapping to modify an HTML document wrt @style and @class -->
  
  <p:input port="source" primary="true"/>
  
  <p:input port="parameters" kind="parameter"/>
  
  <p:output port="_Z_FINAL" primary="true">
    <p:pipe port="result" step="final"/>
  </p:output>
  
  <p:output port="_A_linebroken" primary="false">
    <p:pipe port="result" step="break-paragraphs-at-breaks"/>
  </p:output>
  <p:output port="_C_mapping" primary="false">
    <p:pipe port="result" step="editoria-basic"/>
  </p:output>
  <p:output port="_D_cleanup" primary="false">
    <p:pipe port="result" step="editoria-reduce"/>
  </p:output>
  <p:output port="_E_tuneup" primary="false">
    <p:pipe port="result" step="editoria-tune"/>
  </p:output>
  
  <p:serialization port="_Z_FINAL" indent="true" omit-xml-declaration="true"/>
  

  <!-- Since Editoria doesn't recognize br, we break paragraphs around them. -->
  <p:xslt name="break-paragraphs-at-breaks">
    <p:input port="stylesheet">
      <p:document href="p-split-around-br.xsl"/>
    </p:input>
  </p:xslt>
 
  <p:xslt name="editoria-notes">
    <p:input port="stylesheet">
      <p:document href="editoria-notes.xsl"/>
    </p:input>
  </p:xslt>
  
  
  <!-- Then, the ad-hoc mapping for Editoria. -->
  <p:xslt name="editoria-basic">
    <p:input port="stylesheet">
      <p:document href="editoria-basic.xsl"/>
    </p:input>
  </p:xslt>
  
  <!-- A cleanup pass to enforce global (fallback) mapping rules. -->
  <p:xslt name="editoria-reduce">
    <p:input port="stylesheet">
      <p:document href="editoria-reduce.xsl"/>
    </p:input>
  </p:xslt>
  
  <!-- Final "tuneup" including local editorial pre-processes / macros. -->
  <p:xslt name="editoria-tune">
    <p:input port="stylesheet">
      <p:document href="editoria-tune.xsl"/>
    </p:input>
  </p:xslt>
  
  <p:identity name="final"/>

</p:declare-step>